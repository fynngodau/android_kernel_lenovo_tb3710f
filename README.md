From `9899_osc_build_readme.txt` from the source archive


    How to build this OSC?
    
    $cd kernel-3.18
    $mkdir out
    $export ARCH=arm64
    $export PATH=$PATH:$PWD/gcc/linux-x86/aarch64/aarch64-linux-android-4.9/bin/:$PWD/gcc/linux-x86/arm/arm-linux-androideabi-4.9/bin
    $make O=out wt98999_wifirow_defconfig //wt98999_wifirow_debug_defconfig for eng build
    $make O=out -j8 2>&1 | tee build_kernel.log
